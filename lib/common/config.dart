import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../common/constants.dart';

/// Server config
const serverConfig = {
  "type": "woo",
  "url": "https://app.sartor.io",
  "consumerKey": "ck_5ddb0e3dc3f4fc615bf138cd50495ea7c57309b4",
  "consumerSecret": "cs_4d979c056a12b85c720a6799741475319086415e",
  "blog":
      "http://app.sartor.io", //Your website woocommerce. You can remove this line if it same url
  "forgetPassword": "http://app.sartor.io/wp-login.php?action=lostpassword"
};
const afterShip = {
  "api": "e2e9bae8-ee39-46a9-a084-781d0139274f",
  "tracking_url": "https://sartor.aftership.com"
};

const Payments = {
  "paypal": "assets/icons/payment/paypal.png",
  "stripe": "assets/icons/payment/stripe.png",
  "razorpay": "assets/icons/payment/razorpay.png",
};

/// The product variant config
const ProductVariantLayout = {
  "color": "color",
  "size": "box",
  "height": "option",
};

/// This option is determine hide some components for web
var kLayoutWeb = false;

const kAdvanceConfig = {
  "DefaultLanguage": "en",
  "DefaultCurrency": {
    "symbol": "\$",
    "decimalDigits": 2,
    "symbolBeforeTheNumber": true,
    "currency": "USD"
  },
  "IsRequiredLogin": false,
  "GuestCheckout": false,
  "EnableShipping": true,
  "EnableAddress": true,
  "EnableReview": true,
  "GridCount": 3,
  "DetailedBlogLayout": kBlogLayout.halfSizeImageType,
  "EnablePointReward": false,
  "DefaultPhoneISOCode": "+1",
  "DefaultCountryISOCode": "US",
  "EnableRating": true,
  "EnableSmartChat": true,
  "hideOutOfStock": true,
  'allowSearchingAddress': true,
  "isCaching": false,
  "OnBoardOnlyShowFirstTime": true,
  "EnableConfigurableProduct": false, //for magento
  "EnableAttributesConfigurableProduct": ["color", "size"], //for magento
  "EnableAttributesLabelConfigurableProduct": ["color", "size"], //for magento,
  "EnableAdvertisement": false,
  "Currencies": [
    {"symbol": "\$", "decimalDigits": 2, "symbolBeforeTheNumber": true, "currency": "USD"},
    {"symbol": "\€", "decimalDigits": 2, "symbolBeforeTheNumber": true, "currency": "EUR"},
    {"symbol": "\£", "decimalDigits": 2, "symbolBeforeTheNumber": true, "currency": "GBP"},
  ]
};

/// The Google API Key to support Pick up the Address automatically
/// We recommend to generate both ios and android to restrict by bundle app id
/// The download package is remove these keys, please use your own key
const kGoogleAPIKey = {
  "android": "your-google-api-key",
  "ios": "your-google-api-key",
  "web": "your-google-api-key"
};

// const kOneSignalKey = {
//   'appID': "KONESIGNAL",
// };

/// use to config the product image height for the product detail
/// height=(percent * width-screen)
/// isHero: support hero animate
const kProductDetail = {
  "height": 0.5,
  "marginTop": 0,
  "isHero": false,
  "safeArea": false,
  "showVideo": true,
  "showThumbnailAtLeast": 3,
  "layout": kProductLayout.simpleType
};

/// config for the chat app
 const smartChat = [
 {'app': 'whatsapp://send?phone=123456789123', 'iconData': FontAwesomeIcons.whatsapp},
   {'app': 'tel:123456789123', 'iconData': FontAwesomeIcons.phone},
  {'app': 'sms://123456789123', 'iconData': FontAwesomeIcons.sms}
 ];
const String adminEmail = "admin@sartor.io";

/// the welcome screen data
List onBoardingData = [
  {
    "title": "Welcome to Sartor",
    "image": "assets/images/fogg-delivery-1.png",
    "desc": "Sartor is on the way to serve you. "
  },
  {
    "title": "Connect Surrounding World",
    "image": "assets/images/fogg-uploading-1.png",
    "desc": "See all things happening around you just by a click in your phone. "
        "Fast, convenient and clean."
  },
  {
    "title": "Let's Get Started",
    "image": "fogg-order-completed.png",
    "desc": "Waiting no more, let's see what we get!"
  },
];

const PaypalConfig = {
  "clientId": "AQtxi4BfbfN28bNWI0BHWyl4AE2xMrdF9gFBvxDyqifZ3BZsDzcAUjSXTjXAPvA3HK-VNwsl89HqaEoa",
  "secret": "EKOJYM1WTDAfB-RS2wYGrea-fdlmmpWRfF2ttdurnQkncN2WipH45OMhzIUfEwd4XipCYrbYUrCxrWp7",
  "production": true,
  "paymentMethodId": "paypal",
  "enabled": true,
  "returnUrl": "https://sartor.com/return",
  "cancelUrl": "https://sartor.com/cancel",
};

const TapConfig = {
  "SecretKey": "sk_test_XKokBfNWv6FIYuTMg5sLPjhJ",
  "RedirectUrl": "http://your_website.com/redirect_url",
  "paymentMethodId": "",
  "enabled": false
};

// Limit the country list from Billing Address
const List DefaultCountry = [];
//const List DefaultCountry = [
//  {
//    "name": "Vietnam",
//    "iosCode": "VN",
//    "icon": "https://cdn.britannica.com/41/4041-004-A06CBD63/Flag-Vietnam.jpg"
//  },
//  {
//    "name": "India",
//    "iosCode": "IN",
//    "icon":
//        "https://upload.wikimedia.org/wikipedia/en/thumb/4/41/Flag_of_India.svg/1200px-Flag_of_India.svg.png"
//  },
//  {"name": "Austria", "iosCode": "AT", "icon": ""},
//];

const kAdConfig = {
  "enable": false,
  "type": kAdType.facebookNative,
  // ----------------- Facebook Ads  -------------- //
  "hasdedIdTestingDevice": "ef9d4a6d-15fd-4893-981b-53d87a212c07",
  "bannerPlacementId": "430258564493822_489007588618919",
  "interstitialPlacementId": "430258564493822_489092398610438",
  "nativePlacementId": "430258564493822_489092738610404",
  "nativeBannerPlacementId": "430258564493822_489092925277052",

  // ------------------ Google Admob  -------------- //
  "androidAppId": "ca-app-pub-2101182411274198~6793075614",
  "androidUnitBanner": "ca-app-pub-2101182411274198/4052745095",
  "androidUnitInterstitial": "ca-app-pub-2101182411274198/7131168728",
  "androidUnitReward": "ca-app-pub-2101182411274198/6939597036",
  "iosAppId": "ca-app-pub-2101182411274198~6923444927",
  "iosUnitBanner": "ca-app-pub-2101182411274198/5418791562",
  "iosUnitInterstitial": "ca-app-pub-2101182411274198/9218413691",
  "iosUnitReward": "ca-app-pub-2101182411274198/9026842008",
  "waitingTimeToDisplayInterstitial": 10,
  "waitingTimeToDisplayReward": 10,
};

const kLanguages = [
  {
    "icon": 'assets/images/country/gb.png',
    "text": 'English',
    "code": "en",
  },
  {
    "icon": 'assets/images/country/gb.png',
    "text": 'English',
    "code": "en",
  }
];
